import os
from random import randint
from time import sleep
from django.test import TestCase
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains

# Create your tests here.
class Lab5FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome(os.environ['CHROMEDRIVER_BIN'], chrome_options=chrome_options)
        super(Lab5FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Lab5FunctionalTest, self).tearDown()

    def input_todo(self, todo_title, todo_desc):
        selenium = self.selenium
        # Opening the link we want to test
        selenium.get('http://127.0.0.1:8000/lab-5/')
        # find the form element
        title = selenium.find_element_by_id('id_title')
        description = selenium.find_element_by_id('id_description')

        submit = selenium.find_element_by_id('submit')

        # Fill the form with data
        title.send_keys(todo_title)
        description.send_keys(todo_desc)
        submit.click()


    def test_input_todo(self):
        selenium = self.selenium
        title = 'Functional Test: add todo'
        desc = 'Test' + str(randint(0, 65536))
        self.input_todo(title, desc)
        assert 'Berhasil menambahkan Todo' in selenium.page_source
        assert desc in selenium.page_source

    def test_delete_todo(self):
        selenium = self.selenium
        title = 'Functional Test: delete todo'
        desc = 'Test' + str(randint(0, 65536))
        self.input_todo(title, desc)
        sleep(5)

        # first, mouse over the correct Todo card so the delete button will appears
        todoDiv = selenium.find_element_by_xpath("//div[contains(text(), '"+ desc +"')]")
        hover = ActionChains(selenium).move_to_element(todoDiv)
        hover.perform()
        sleep(4)

        # now, find the correct delete button using XPATH
        delete = selenium.find_element_by_xpath("//div[contains(text(), '"+ desc +"')]/../div[@class='to-do-list-delete']/div[@class='to-do-list-delete-button']")

        # hover first!
        hover = ActionChains(selenium).move_to_element(delete)
        hover.perform()
        sleep(4)

        # CLICK!!!
        delete.click()

        # assert that the newly created Todo is now deleted
        assert desc not in selenium.page_source
