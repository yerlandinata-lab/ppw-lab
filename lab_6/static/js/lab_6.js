/**
 * Lab 6 by Yudhistira Erlandinata
 */
var print;
var chatBox;
var chats;
var calculator;
var calculatorError = false;
var chatHead;
var dropdownSelection;
var conversationState = '';
var navbar;
var navbarBrand;
const ENTER = 13;
const corsProxy = 'https://cors-anywhere.herokuapp.com/';
const botEndPoint = 'http://www.cleverbot.com/getreply?';
const notSoSecret = 'CC59plCRyRXFeEyYaRru8L_RiiA';
const themes = [
    {id:0,text:'Red',bcgColor:'#F44336',fontColor:'#FAFAFA'},
    {id:1,text:'Pink',bcgColor:'#E91E63',fontColor:'#FAFAFA'},
    {id:2,text:'Purple',bcgColor:'#9C27B0',fontColor:'#FAFAFA'},
    {id:3,text:'Indigo',bcgColor:'#3F51B5',fontColor:'#FAFAFA'},
    {id:4,text:'Blue',bcgColor:'#2196F3',fontColor:'#212121'},
    {id:5,text:'Teal',bcgColor:'#009688',fontColor:'#212121'},
    {id:6,text:'Lime',bcgColor:'#CDDC39',fontColor:'#212121'},
    {id:7,text:'Yellow',bcgColor:'#FFEB3B',fontColor:'#212121'},
    {id:8,text:'Amber',bcgColor:'#FFC107',fontColor:'#212121'},
    {id:9,text:'Orange',bcgColor:'#FF5722',fontColor:'#212121'},
    {id:10,text:'Brown',bcgColor:'#795548',fontColor:'#FAFAFA'}
];

const defaultTheme = {id:0,text:'Indigo',bcgColor:'#3F51B5',fontColor:'#FAFAFA'};

/**
 * Sends message and renders it
 * @param {*} event 
 */
const sendMessage = function(event){
    if (event.which == ENTER) {
        chats.append(createMessage(true, chatBox.val()));
        talkToBot(chatBox.val());
        chatBox.val('');
    }
};

/**
 * Talks to Cleverbot service via cors-anywhere proxy
 * @param {*} message 
 */
const talkToBot = function(message){
    const param = jQuery.param({
        key: notSoSecret,
        input: message,
        cs: conversationState
    });
    axios.get(corsProxy + botEndPoint + param).then(response => {
        conversationState = response.data.cs;
        chats.append(createMessage(false, response.data.output));
    });
};

/**
 * Creates html text
 * @param {*} isSending determine the bg-color and style 
 * @param {*} message 
 */
const createMessage = function(isSending, message){
    const style = isSending ? 'msg-send' : 'msg-receive';
    return '<div class="' + style + '">' + message + '</div>';
};


/**
 * Select theme from dropdown
 */
const selectTheme = function(){
    const selected = dropdownSelection.select2('data')[0];
    const theme = {
        id: selected.id,
        text: selected.text,
        bcgColor: selected.bcgColor,
        fontColor: selected.fontColor
    };
    localStorage.selectedTheme = JSON.stringify(theme);
    applyTheme(theme);
};

/**
 * Set the theme
 * @param {*} theme bcgColor: background color, fontColor: text color
 */
const applyTheme = function(theme){
    calculator.css('background-color', theme.bcgColor);
    calculator.find('#title').css('color', theme.fontColor);
    chatHead.css('background-color', theme.bcgColor);
    chatHead.find('h2').css('color', theme.fontColor);
    navbar.css('background-color', theme.bcgColor);
    navbarBrand.css('color', theme.fontColor);
};

/**
 * Invoked when document is fully loaded
 */
$(document).ready(() => {
    initLocalStorage();
    print = $('#print');
    chats = $('.msg-insert');
    chatBox = $('#chat-textarea');
    calculator = $('.model');
    chatHead = $('.chat-head');
    dropdownSelection = $('#my-select');
    navbar = $('.navbar');
    navbarBrand = $('.navbar-brand');
    chatBox.keypress(sendMessage);
    dropdownSelection.select2({
        'data': JSON.parse(localStorage.themes)
    });
    $('#apply').on('click', selectTheme);
    applyTheme(JSON.parse(localStorage.selectedTheme));
    dropdownSelection.val(JSON.parse(localStorage.selectedTheme).id);
    dropdownSelection.trigger('change');  
    unitTest();
});

/**
 * Create new localstorage object if none exists
 */
const initLocalStorage = function(){
    if (!localStorage.themes){
        localStorage.themes = JSON.stringify(themes);
        localStorage.selectedTheme = JSON.stringify(defaultTheme);
    }
};

const go = x => {
    if (x === 'ac') {
        print.val('');
    } else if (x === 'eval') {
        var result;
        try{ //avoid js errors
            result = Math.round(evaluate(print.val()) * 10000) / 10000;
        } catch (error){
            result = 'Syntax error';
            calculatorError = true;
        }
        if (result == Infinity || isNaN(result)) result = 'undefined'; 
        print.val(result);
    } else {
        if (calculatorError) {
            print.val(x);
            calculatorError = false;
        }
        else print.val(print.val() + x);
    }
};

function evaluate(fn) {
    return new Function('return ' + fn)();
}
