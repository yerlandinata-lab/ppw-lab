from django.views.generic import TemplateView
from django.shortcuts import render

# Create your views here.
class Lab6View(TemplateView):
    template_name = 'lab_6/lab_6.html'

    def get(self, request, *args, **kwargs):
        response = {}
        response['author'] = 'Yudhistira Erlandinata'
        return render(request, Lab6View.template_name, response)
