from django.conf.urls import url
from .views import Lab6View

urlpatterns = [
    url(r'^$', Lab6View.as_view()),
]