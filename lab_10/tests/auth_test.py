from django.test import TestCase
from lab_10 import auth

class AuthUnitTest(TestCase):

    def test_login(self):
        def fake_login(request, redirect=None):
            return True
        auth.auth_login = fake_login
        self.assertTrue(auth.login(None, redirect=None))

    def test_logout(self):
        def fake_logout(request, redirect=None):
            return True
        auth.auth_logout = fake_logout
        self.assertTrue(auth.logout(None))
    