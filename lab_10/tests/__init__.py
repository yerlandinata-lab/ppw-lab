from .views_tests import ViewsUnitTest
from .models_tests import ModelsUnitTest
from .auth_test import AuthUnitTest
from .omdb_api_wrapper_tests import OmdbApiWrapperUnitTest
