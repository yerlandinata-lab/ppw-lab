from django.test import TestCase
from django.urls import reverse
from lab_10 import views
from lab_10.models import Pengguna, MovieKu

class ViewsUnitTest(TestCase):

    def setUp(self):
        self.original_omdb_get_movie_detail = views.get_detail_movie
        self.original_omdb_search_movie = views.search_movie
        def fake_detail_movie(id):
            return True
        def fake_search_movie(judul, tahun):
            return []
        views.get_detail_movie = fake_detail_movie
        views.search_movie = fake_search_movie

    def tearDown(self):
        views.get_detail_movie = self.original_omdb_get_movie_detail
        views.search_movie = self.original_omdb_search_movie

    def test_render_index(self):
        response = self.client.get(reverse('lab-10:index'))
        self.assertRedirects(response, reverse('lab-10:dashboard'))

    def test_dashboard_not_logged_in(self):
        response = self.client.get(reverse('lab-10:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertIn('login', response.content.decode('utf8'))

    def test_dashboard_logged_in(self):
        self.__login()
        response = self.client.get(reverse('lab-10:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('login', response.content.decode('utf8'))

    def test_dashboard_login_add_session_movies(self):
        session = self.__login()
        session['movies'] = ['123']
        session.save()
        response = self.client.get(reverse('lab-10:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('login', response.content.decode('utf8'))

    def test_dashboard_logged_in_new_user(self):
        session = self.__login()
        session['kode_identitas'] = 'aaa'
        session.save()
        response = self.client.get(reverse('lab-10:dashboard'))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('login', response.content.decode('utf8'))
        self.assertEqual(2, Pengguna.objects.all().count())

    def test_movielist_not_logged_in(self):
        response = self.client.get(reverse('lab-10:movie_list'))
        self.assertEqual(response.status_code, 200)
        self.assertIn('List Movie', response.content.decode('utf8'))

    def test_movielist_logged_in(self):
        self.__login()
        response = self.client.get(reverse('lab-10:movie_list'))
        self.assertEqual(response.status_code, 200)
        self.assertIn('logout', response.content.decode('utf8'))
        self.assertIn('List Movie', response.content.decode('utf8'))

    def test_movielist_search(self):
        judul = 'avengers'
        tahun = '2015'
        response = self.client.get(reverse('lab-10:movie_list') + '?judul={}&tahun={}'.format(judul, tahun))
        self.assertEqual(response.status_code, 200)
        self.assertIn('List Movie', response.content.decode('utf8'))
        self.assertIn(judul, response.content.decode('utf8'))
        self.assertIn(tahun, response.content.decode('utf8'))

    def test_movie_detail_logged_in(self):
        self.__login()
        response = self.client.get(reverse('lab-10:movie_detail', args=('213123', )))
        self.assertEqual(response.status_code, 200)
        self.assertNotIn('login', response.content.decode('utf8'))

    def test_movie_detail_not_logged_in(self):
        response = self.client.get(reverse('lab-10:movie_detail', args=('213123', )))
        self.assertEqual(response.status_code, 200)

    def test_add_movie_to_watchlist_model(self):
        self.__login()
        movie_id = '123456'
        response = self.client.get(reverse('lab-10:add_watch_later', args=(movie_id, )))
        self.assertRedirects(response, reverse('lab-10:movie_detail', args=(movie_id, )))
        self.assertTrue(MovieKu.objects.all()[0])

    def test_add_movie_to_watchlist_session(self):
        movie_id = '123456'
        response = self.client.get(reverse('lab-10:add_watch_later', args=(movie_id, )))
        self.assertRedirects(response, reverse('lab-10:movie_detail', args=(movie_id, )))
        self.assertFalse(MovieKu.objects.all())

    def test_add_movie_to_watchlist_session_append(self):
        session = self.client.session
        session['movies'] = ['123']
        session.save()
        movie_id = '123456'
        response = self.client.get(reverse('lab-10:add_watch_later', args=(movie_id, )))
        self.assertRedirects(response, reverse('lab-10:movie_detail', args=(movie_id, )))
        self.assertFalse(MovieKu.objects.all())

    def test_watchlist_model(self):
        self.__login()
        movie_id = '123456'
        user = Pengguna()
        user.kode_identitas = '333'
        user.save()
        MovieKu.objects.create(kode_movie=movie_id, pengguna=user)
        response = self.client.get(reverse('lab-10:list_watch_later'))
        self.assertEqual(response.status_code, 200)

    def test_watchlist_session(self):
        session = self.client.session
        movie_id = '123456'
        session['movies'] = [movie_id, ]
        session.save()
        response = self.client.get(reverse('lab-10:list_watch_later'))
        self.assertEqual(response.status_code, 200)

    def test_movie_search_api(self):
        response = self.client.get(reverse('lab-10:api_search_movie', args=('a', '1', )))
        self.assertEqual(response.status_code, 200)

    def test_watch_movie(self):
        self.__login()
        movie_id = '123456'
        user = Pengguna.objects.get(kode_identitas='123')
        MovieKu.objects.create(kode_movie=movie_id, pengguna=user)
        response = self.client.post(reverse('lab-10:watch_movie'), data={'id': movie_id})
        self.assertEqual(response.status_code, 201)
        self.assertTrue(MovieKu.objects.filter(kode_movie=movie_id)[0].is_watched)

    def __login(self):
        '''
        Session testing procedure:
        assign the client.session to a variable, let it be s
        to set a new session value, add it to s, treat s as a dict,
        invoke s.save()
        there you go, client.session is updated #there is no other way around - nonsense, but important to keep this in mind
        '''
        user = 'a_user'
        kode = '123'
        Pengguna.objects.create(kode_identitas=kode)
        session = self.client.session
        session['user_login'] = user
        session['kode_identitas'] = kode
        session['role'] = 'user'
        session.save()
        return session

