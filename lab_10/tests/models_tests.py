from django.test import TestCase
from lab_10.models import Pengguna, MovieKu

class ModelsUnitTest(TestCase):

    def test_pengguna(self):
        npm = '123'
        nama = 'aaa'
        user = Pengguna.objects.create(kode_identitas=npm, nama=nama)
        self.assertEqual(str(user), nama + '(' + npm + ')')

    def test_movie(self):
        npm = '123'
        nama = 'aaa'
        user = Pengguna.objects.create(kode_identitas=npm, nama=nama)
        code = '321'
        movie = MovieKu.objects.create(pengguna=user, kode_movie=code)
        self.assertEqual(str(movie), code + '[' + str(user) +']')
