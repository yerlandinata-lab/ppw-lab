from django.test import TestCase
from lab_10.apis import omdb

class OmdbApiWrapperUnitTest(TestCase):

    def setUp(self):
        self.original_get = omdb.requests.get
        class FakeResponse:
            def json(self):
                return {'Response': 'True', 'totalResults': '5', 'Search': '[1,2,3]'}
        def fake_get(url):
            return FakeResponse()
        omdb.requests.get = fake_get

    def tearDown(self):
        omdb.requests.get = self.original_get

    def test_get_detail_movie(self):
        self.assertTrue(omdb.get_detail_movie('2'))

    def test_search_movie(self):
        self.assertTrue(omdb.search_movie('avengers', '2018'))