from __future__ import unicode_literals

import json

from django.contrib import messages
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render
from django.urls import reverse

from .apis.omdb import get_detail_movie, search_movie
from .utils import *
response = {'lab_author':'Yudhistira E'}

# Create your views here.

### USER
def index(request):
    # print ("#==> masuk index")
    return HttpResponseRedirect(reverse('lab-10:dashboard'))

def dashboard(request):
    html = 'lab_10/dashboard.html'
    response['user_name'] = get_data_user(request, 'user_login')
    if not response['user_name']:
        return render(request, html, response)
    set_data_for_session(request)
    kode_identitas = get_data_user(request, 'kode_identitas')
    try:
        pengguna = Pengguna.objects.get(kode_identitas = kode_identitas)
    except Exception as e:
        pengguna = create_new_user(request)
    movies_id = get_my_movies_from_session(request)
    save_movies_to_database(pengguna, movies_id)
    return render(request, html, response)

### MOVIE : LIST and DETAIL
def movie_list(request):
    html = 'lab_10/movie/list.html'
    response['user_name'] = get_data_user(request, 'user_login')
    if response['user_name']:
        set_data_for_session(request)
    judul, tahun = get_parameter_request(request)
    urlDataTables = "/lab-10/api/movie/" + judul + "/" + tahun
    jsonUrlDT = json.dumps(urlDataTables)
    response['jsonUrlDT'] = jsonUrlDT
    response['judul'] = judul
    response['tahun'] = tahun

    get_data_session(request)

    return render(request, html, response)

def movie_detail(request, id):
    # print ("MOVIE DETAIL = ", id)
    response['id'] = id
    response['user_name'] = get_data_user(request, 'user_login')
    if get_data_user(request, 'user_login'):
        is_added = check_movie_in_database(request, id)
        set_data_for_session(request)
    else:
        is_added = check_movie_in_session(request, id)
    response['added'] = is_added
    response['movie'] = get_detail_movie(id)
    html = 'lab_10/movie/detail.html'
    return render(request, html, response)

### WATCH LATER : ADD and LIST
def add_watch_later(request, id):
    # print ("ADD WL => ", id)
    msg = "Berhasil tambah movie ke Watch Later"
    if get_data_user(request, 'user_login') and not check_movie_in_database(request, id):
        add_item_to_database(request, id)
    elif not check_movie_in_session(request, id):
        add_item_to_session(request, id)
    ### REMOVED: MESSAGE HACKING DETECTED message -- mengurangi kompleksitas testing
    messages.success(request, msg)
    return HttpResponseRedirect(reverse('lab-10:movie_detail', args=(id,)))

def list_watch_later(request):
    #  Implement this function by yourself
    get_data_session(request)
    moviesku = []
    if get_data_user(request, 'user_login'):
        moviesku = get_my_movies_from_database(request)
    else:
        moviesku = get_my_movies_from_session(request)

    watch_later_movies = get_list_movie_from_api(moviesku)

    response['watch_later_movies'] = watch_later_movies
    for movie in watch_later_movies:
        movie['is_watched'] = MovieKu.objects.get(kode_movie=movie['imdbid']).is_watched if 'imdbid' in movie else False
    html = 'lab_10/movie/watch_later.html'
    return render(request, html, response)

### SESSION : GET and SET
def get_data_session(request):
    if get_data_user(request, 'user_login'):
        response['author'] = get_data_user(request, 'user_login')

def set_data_for_session(request):
    response['author'] = get_data_user(request, 'user_login')
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']

def watch_movie(request):
    kode_identitas = get_data_user(request, 'kode_identitas')
    pk = request.POST['id'] if 'id' in request.POST else None
    if pk and kode_identitas and check_movie_in_database(request, pk):
        movie = MovieKu.objects.filter(kode_movie=pk)[0]
        movie.is_watched = True
        movie.save()
        return HttpResponse(status=201)

### API : SEARCH movie
def api_search_movie(request, judul, tahun):
    search_results = search_movie(judul, tahun)
    items = search_results
    dataJson = json.dumps({"dataku":items})
    mimetype = 'application/json'
    return HttpResponse(dataJson, mimetype)

