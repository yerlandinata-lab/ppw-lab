from lab_9.auth import auth_login, auth_logout

def login(request, redirect='lab-10:index'):
    return auth_login(request, redirect=redirect)

def logout(request):
    return auth_logout(request, redirect='lab-10:index')
