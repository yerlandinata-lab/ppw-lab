from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
#catatan: tidak bisa menampilkan messages jika bukan menggunakan method 'render'
from .apis.enterkomputer import get_drones, get_opticals, get_soundcards

response = {'lab_author': 'Yudhistira Erlandinata'}
favorites_key = ('fav_drones', 'fav_opticals', 'fav_soundcards')

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

# ======================================================================== #
# User Func
# Cek status login, jika login arahkan ke halaman profile, jika tidak render halaman login 
def index(request):
    # print("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('lab-9:profile'))
    else:
        html = 'lab_9/session/login.html'
        return render(request, html, response)

def set_data_for_session(res, request):
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['drones'] = get_drones().json()
    response['opticals'] = get_opticals().json()
    response['soundcards'] = get_soundcards().json()
    ## handling agar tidak error saat pertama kali login (session kosong)
    for key in favorites_key:
        if key in request.session.keys():
            response[key] = request.session[key]
        else:
            response[key] = []

def profile(request):
    # print("#==> profile")
    ## sol : bagaimana cara mencegah error, jika url profile langsung diakses
    if 'user_login' not in request.session.keys():
        return HttpResponseRedirect(reverse('lab-9:index'))
    ## end of sol

    set_data_for_session(response, request)

    html = 'lab_9/session/profile.html'
    return render(request, html, response)

# ======================================================================== #

### General Function
def add_session_item(request, key, id):
    ssn_key = request.session.keys()
    if not key in ssn_key and key in favorites_key: # enhance security
        request.session[key] = [id]
    elif id not in request.session[key]:
        items = request.session[key]
        items.append(id)
        request.session[key] = items

    msg = "Berhasil tambah " + key +" favorite"
    messages.success(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))

def del_session_item(request, key, id):
    if key in favorites_key:  # enhance security
        items = request.session[key]
        items.remove(id)
        request.session[key] = items

        msg = "Berhasil hapus item " + key + " dari favorite"
        messages.error(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))

def clear_session_item(request, key):
    if key in favorites_key:  # enhance security
        request.session[key] = []
        msg = "Berhasil hapus session : favorite " + key
        messages.error(request, msg)
    return HttpResponseRedirect(reverse('lab-9:profile'))



# ======================================================================== #
# COOKIES

# Cek status login melalui cookies
def cookie_login(request):
    # print("#==> masuk login")
    if cookie_auth(request):
        return HttpResponseRedirect(reverse('lab-9:cookie_profile'))
    else:
        html = 'lab_9/cookie/login.html'
        return render(request, html, response)

def cookie_auth_login(request):
    # print("# Auth login")
    if request.method == "POST":
        user_login = request.POST['username']
        user_password = request.POST['password']

        if auth(user_login, user_password):
            # print("#SET cookies")
            res = HttpResponseRedirect(reverse('lab-9:cookie_profile'))

            res.set_cookie('user_login', user_login)
            res.set_cookie('user_password', user_password)

            return res
        else:
            msg = "Username atau Password Salah"
            messages.error(request, msg)
            return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))

def cookie_profile(request):
    # print("# cookie profile ")
    # method ini untuk mencegah error ketika akses URL secara langsung
    if not cookie_auth(request):
        # print("belum login")
        return HttpResponseRedirect(reverse('lab-9:cookie_login'))
    else:
        html = "lab_9/cookie/profile.html"
        return render(request, html, response)

def cookie_clear(request):
    res = HttpResponseRedirect(reverse('lab-9:cookie_login'))
    res.delete_cookie('user_password')
    res.delete_cookie('user_login')

    msg = "Anda berhasil logout. Cookies direset"
    messages.info(request, msg)
    return res

# authentication
def auth(in_uname, in_pwd):
    my_uname = "utest" 
    my_pwd = "ptest" 
    return in_uname == my_uname and in_pwd == my_pwd

# cookie-based authentication
def cookie_auth(request):
    return 'user_login' in request.COOKIES and 'user_password' in request.COOKIES and auth(request.COOKIES['user_login'], request.COOKIES['user_password'])
