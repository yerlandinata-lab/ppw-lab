from django.test import TestCase

from lab_9.apis import enterkomputer

class EnterKomputerApiUnitTest(TestCase):
    
    def setUp(self):
        super().setUp()
        # mock dependency for faster testing: requests.get
        self.original_get_request = enterkomputer.requests.get
        def fake_request(url):
            return True
        enterkomputer.requests.get = fake_request

    def tearDown(self):
        enterkomputer.requests.get = self.original_get_request

    def test_get_drones(self):
        self.assertTrue(enterkomputer.get_drones())

    def test_get_opticals(self):
        self.assertTrue(enterkomputer.get_opticals())

    def test_get_soundcard(self):
        self.assertTrue(enterkomputer.get_soundcards())
        
