from .views_unit_tests import Lab9ViewsUnitTest
from .enterkomputer_unit_tests import EnterKomputerApiUnitTest
from .auth_unit_tests import AuthUnitTest
from .csui_unit_tests import CsuiApiUnitTest
