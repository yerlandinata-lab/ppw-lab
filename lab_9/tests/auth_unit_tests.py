from django.test import TestCase
from django.urls import reverse

from lab_9 import auth

class AuthUnitTest(TestCase):
    
    def setUp(self):
        super().setUp()
        # mock dependency for faster testing: requests.get
        self.original_get_access_token = auth.get_access_token
        def fake_get_access_token(user, password):
            if user and password:
                return True
        auth.get_access_token = fake_get_access_token
        self.original_verify_user = auth.verify_user
        def fake_verify_user(access_token):
            return {'identity_number':'123', 'role':'superuser'}
        auth.verify_user = fake_verify_user

    def tearDown(self):
        auth.get_access_token = self.original_get_access_token
        auth.verify_user = self.original_verify_user

    def test_post_login_success(self):
        response = self.client.post(reverse('lab-9:auth_login'), {'username':'utest', 'password':'ptest'})
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:index'))
        self.assertIn('user_login', self.client.session)

    def test_post_login_fail(self):
        response = self.client.post(reverse('lab-9:auth_login'), {'username':'', 'password':''})
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:index'))
        self.assertNotIn('user_login', self.client.session)

    def test_logout(self):
        session = self.client.session
        session['user_login'] = 'a'
        session.save()
        response = self.client.post(reverse('lab-9:auth_logout'), {'username':'', 'password':''})
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:index'))
        self.assertNotIn('user_login', self.client.session)
