from django.test import TestCase

from lab_9.apis import csui

class CsuiApiUnitTest(TestCase):
    
    def setUp(self):
        super().setUp()
        # mock dependency for faster testing: requests.get
        self.original_get_request = csui.requests.get
        self.original_post_request = csui.requests.post
        class FakeResponse:
            def json(self):
                return {'access_token':'123qwerty'}
        def fake_get_request(url, params=None):
            return FakeResponse()
        def fake_post_request(url, data=None, params=None, headers=None):
            return FakeResponse()
        csui.requests.get = fake_get_request
        csui.requests.post = fake_post_request

    def tearDown(self):
        csui.requests.get = self.original_get_request
        csui.requests.post = self.original_post_request

    def test_get_access_token_success(self):
        self.assertTrue(csui.get_access_token('uname', 'pass'))

    def test_get_access_token_fail(self):
        self.assertFalse(csui.get_access_token(None, None))

    def test_get_client_id(self):
        self.assertTrue(csui.get_client_id())
    
    def test_verify_user(self):
        self.assertTrue(csui.verify_user('dfq31x'))

    def test_get_data_user(self):
        self.assertTrue(csui.get_data_user('3124f', '134fd'))
