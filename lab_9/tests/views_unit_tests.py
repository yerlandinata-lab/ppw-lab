from django.test import TestCase
from django.urls import reverse

from lab_9 import views

# Create your tests here.

class Lab9ViewsUnitTest(TestCase):

    def setUp(self):
        super().setUp()
        self.original_get_drones = views.get_drones
        self.stub_lab_9_enterkomputer_apis()

    def stub_lab_9_enterkomputer_apis(self):
        '''
        Faking the api call for faster unit tests,
        the APIs will be tested independently
        '''
        class FakeResponse:
            def json(self):
                return {}
        def fake_api_gets():
            return FakeResponse()
        views.get_drones = fake_api_gets
        views.get_opticals = fake_api_gets
        views.get_soundcards = fake_api_gets
    
    def tearDown(self):
        super().tearDown()
        views.get_drones = self.original_get_drones    
        self.client.cookies.clear()

    def test_session_index_not_logged_in(self):
        response = self.client.get(reverse('lab-9:index'))
        response_html = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200) # OK
        self.assertTemplateUsed(response, 'lab_9/session/login.html')

    def test_session_index_logged_in(self):
        self.__session_login()
        response = self.client.get(reverse('lab-9:index'))
        self.assertEqual(response.status_code, 302) # Redirected
        self.assertEqual(response.url, reverse('lab-9:profile')) # Redirected to profile page

    def test_session_profile_not_logged_in(self):
        response = self.client.get(reverse('lab-9:profile'))
        response_html = response.content.decode('utf8')
        self.assertEqual(response.status_code, 302) # Redirected
        self.assertEqual(response.url, reverse('lab-9:index')) # Redirected to login page (index)

    def test_session_profile_logged_in_with_fav_drones(self):
        session = self.__session_login_sso()
        drones = ['a', 'b']
        session['fav_drones'] = drones
        session.save()
        response = self.client.get(reverse('lab-9:profile'))
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.context['fav_drones'], drones)

    def test_session_profile_logged_in_without_fav_drones(self):
        session = self.__session_login_sso()
        response = self.client.get(reverse('lab-9:profile'))
        self.assertEqual(response.status_code, 200) # OK
        self.assertEqual(response.context['fav_drones'], [])

    def test_add_session_item_new(self):
        response = self.client.get(reverse('lab-9:add_session_item', args=('fav_drones', 666)))
        self.assertEqual(self.client.session['fav_drones'], ['666'])
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:profile')) # redirected to profile

    def test_add_session_item_append(self):
        session = self.client.session
        fav_drones = ['1', '2', '3']
        session['fav_drones'] = fav_drones
        session.save()
        response = self.client.get(reverse('lab-9:add_session_item', args=('fav_drones', 666)))
        self.assertEqual(self.client.session['fav_drones'], fav_drones + ['666'])
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:profile')) # redirected to profile

    def test_del_session_item(self):
        session = self.client.session
        fav_drones = ['1', '2', '3']
        session['fav_drones'] = fav_drones
        session.save()
        response = self.client.get(reverse('lab-9:del_session_item', args=('fav_drones', 2)))
        self.assertNotIn(2, self.client.session['fav_drones'])
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:profile')) # redirected to profile

    def test_clear_session_drones(self):
        session = self.client.session
        fav_drones = ['1', '2', '3']
        session['fav_drones'] = fav_drones
        session.save()
        response = self.client.get(reverse('lab-9:clear_session_item', args=('fav_drones', )))
        self.assertEqual(self.client.session['fav_drones'], [])
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:profile')) # redirected to profile

    def __session_login_sso(self):
        session = self.__session_login()
        session['access_token'] = ''
        session['kode_identitas'] = ''
        session['role'] = ''
        session.save()
        return session

    def __session_login(self):
        '''
        Session testing procedure:
        assign the client.session to a variable, let it be s
        to set a new session value, add it to s, treat s as a dict,
        invoke s.save()
        there you go, client.session is updated #there is no other way around - nonsense, but important to keep this in mind
        '''
        user = 'a_user'
        session = self.client.session
        session['user_login'] = user
        session.save()
        return session


    def test_cookie_index_logged_in(self):
        self.__cookie_login()
        response = self.client.get(reverse('lab-9:cookie_login'))
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:cookie_profile')) # redirected to cookie profile
    
    def test_cookie_index_not_logged_in(self):
        response = self.client.get(reverse('lab-9:cookie_login'))
        response_html = response.content.decode('utf8')
        self.assertEqual(response.status_code, 200) # OK
        self.assertTemplateUsed(response, 'lab_9/cookie/login.html')

    def test_cookie_auth_post_success(self):
        response = self.client.post(reverse('lab-9:cookie_auth_login'), {'username':'utest', 'password':'ptest'})
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:cookie_profile')) # redirected to cookie profile
        self.assertIn('user_login', self.client.cookies)
        self.assertIn('user_password', self.client.cookies)

    def test_cookie_auth_post_fail(self):
        response = self.client.post(reverse('lab-9:cookie_auth_login'), {'username':'', 'password':''})
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:cookie_login')) # redirected to cookie login
        response = self.client.get(response.url)
        self.assertEqual(response.status_code, 200)
        self.assertIn('Username atau Password Salah', response.content.decode('utf8'))

    def test_cookie_auth_get(self):
        response = self.client.get(reverse('lab-9:cookie_auth_login'))
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:cookie_login')) # redirected to cookie login

    def test_cookie_profile_logged_in(self):
        self.__cookie_login()
        response = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertEqual(response.status_code, 200) #OK
        self.assertTemplateUsed(response, 'lab_9/cookie/profile.html')

    def test_cookie_profile_not_logged_in(self):
        response = self.client.get(reverse('lab-9:cookie_profile'))
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:cookie_login')) # redirected to cookie login

    def test_cookie_logout(self):
        self.__cookie_login()
        response = self.client.get(reverse('lab-9:cookie_clear'))
        self.assertEqual(response.status_code, 302) # redirected
        self.assertEqual(response.url, reverse('lab-9:cookie_login')) # redirected to cookie login
        self.assertFalse(self.client.cookies['user_login'].value)
        self.assertFalse(self.client.cookies['user_password'].value)

    def __cookie_login(self):
        self.client.cookies['user_login'] = 'utest'
        self.client.cookies['user_password'] = 'ptest'
