import requests

DRONE_API       = 'https://www.enterkomputer.com/api/product/drone.json'
SOUNDCARD_API   = 'https://www.enterkomputer.com/api/product/soundcard.json'
OPTICAL_API     = 'https://www.enterkomputer.com/api/product/optical.json'

def get_drones():
    return requests.get(DRONE_API)

def get_opticals():
    return requests.get(OPTICAL_API)

def get_soundcards():
    return requests.get(SOUNDCARD_API)
