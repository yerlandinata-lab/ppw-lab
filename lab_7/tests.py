import json
from django.test import TestCase, Client
from .models import Friend
from lab_7.api_cs_ui_helper import CSUIhelper

# Create your tests here.
class Lab7UnitTest(TestCase):

    def test_lab7_exists(self):
        response = Client().get('/lab-7/')
        self.assertEqual(response.status_code, 200)

    def test_csui_mahasiswa_list(self):
        response = Client().get('/lab-7/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Tidak ada mahasiswa', html_response)
    
    def test_csui_invalid_page(self):
        response = Client().get('/lab-7/students/page/999/')
        html_response = response.content.decode('utf8')
        self.assertNotIn('Tidak ada mahasiswa', html_response)

    def test_csui_get_mahasiswa_by_npm(self):
        student = CSUIhelper().instance.get_mahasiswa('1606894534')
        self.assertEqual(student['nama'], 'YUDHISTIRA ERLANDINATA')

    def test_csui_get_mahasiswa_by_npm_notexist(self):
        student = CSUIhelper().instance.get_mahasiswa('69696969669')
        self.assertIsNone(student)

    def test_friend_representation(self):
        Friend.objects.create(name='test', npm='npmtest')
        self.assertEqual(str(Friend.objects.all()[0]), 'test, npmtest')

    def test_get_friend_list(self):
        name1 = 'name1'
        name2 = 'name2'
        npm1 = 'npm1'
        npm2 = 'npm2'
        Friend.objects.create(name=name1, npm=npm1)
        Friend.objects.create(name=name2, npm=npm2)
        response = Client().get('/lab-7/friends')
        json_response = json.loads(response.json())
        self.assertEqual(json_response[0]['name'], name1)
        self.assertEqual(json_response[1]['name'], name2)
        self.assertEqual(json_response[0]['npm'], npm1)
        self.assertEqual(json_response[1]['npm'], npm2)

    def test_get_friend_by_npm(self):
        name1 = 'name1'
        npm1 = '2131323'
        Friend.objects.create(name=name1, npm=npm1)
        response = Client().get('/lab-7/friends/npm/' + npm1 + '/')
        self.assertEqual(response.status_code, 200) # ok, found
        json_response = json.loads(response.json())
        self.assertEqual(json_response['name'], name1)
        self.assertEqual(json_response['npm'], npm1)

    def test_get_friend_by_npm_not_found(self):
        response = Client().get('/lab-7/friends/npm/13223/')
        self.assertEqual(response.status_code, 404) # not found

    def test_post_friend(self):
        response = Client().post('/lab-7/friends', data={'name':'test', 'npm':'npmtest'})
        self.assertEqual(response.status_code, 201) # new resource created
        self.assertEqual(Friend.objects.all()[0].name, 'test')
        self.assertEqual(Friend.objects.all()[0].npm, 'npmtest')

    def test_post_duplicate_friend(self):
        response = Client().post('/lab-7/friends', data={'name':'test', 'npm':'npmtest'})
        self.assertEqual(response.status_code, 201) # new resource created
        response = Client().post('/lab-7/friends', data={'name':'test', 'npm':'npmtest'})
        self.assertEqual(response.status_code, 400) # npm already exist
        self.assertEqual(len(Friend.objects.all()), 1)

    def test_delete_friend(self):
        name1 = 'name1'
        npm1 = '2131323'
        Friend.objects.create(name=name1, npm=npm1)
        response = Client().delete('/lab-7/friends/npm/' + npm1 + '/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(Friend.objects.filter(npm=npm1)), 0)

    def test_delete_friend_not_exist(self):
        name1 = 'name1'
        npm1 = '2131323'
        Friend.objects.create(name=name1, npm=npm1)
        response = Client().delete('/lab-7/friends/npm/12321321/')
        self.assertEqual(response.status_code, 404)
        self.assertEqual(len(Friend.objects.filter(npm=npm1)), 1)

    def test_cannot_delete_all_friend(self):
        response = Client().delete('/lab-7/friends')
        self.assertEqual(response.status_code, 400)

    def test_friend_list_view(self):
        response = Client().get('/lab-7/friend-list')
        html_response = response.content.decode('utf8')
        self.assertIn('Friend Fasilkom', html_response)

    def test_get_friend_detail_not_in_csui(self):
        name1 = 'name1'
        npm1 = '2131323'
        Friend.objects.create(name=name1, npm=npm1)
        response = Client().get('/lab-7/friend-detail/' + npm1 +'/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(name1, response.content.decode('utf8'))

    def test_get_friend_detail_in_csui(self):
        name1 = 'YUDHISTIRA ERLANDINATA'
        npm1 = '1606894534'
        Friend.objects.create(name=name1, npm=npm1)
        response = Client().get('/lab-7/friend-detail/' + npm1 +'/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(name1, response.content.decode('utf8'))
        self.assertIn('Jakarta', response.content.decode('utf8'))

    def test_get_friend_detail_notfound(self):
        response = Client().get('/lab-7/friend-detail/121/')
        self.assertEqual(response.status_code, 404)
