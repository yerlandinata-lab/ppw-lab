import json
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.core import serializers

from lab_7.models import Friend
from lab_7.api_cs_ui_helper import CSUIhelper

AUTHOR = 'Yudhistira Erlandinata'
MAX_PAGE = 68

class StudentsView(TemplateView):

    template_name = 'lab_7/lab_7.html'

    def get(self, request, page=1, *args, **kwargs):
        page = int(page)
        if page < 1 or page > MAX_PAGE: # max page 68
            page = 1
        data = serializers.serialize('json', Friend.objects.all())
        friends = json.loads(data)
        friends = [f['fields'] for f in friends]
        mahasiswa_list = CSUIhelper().instance.get_mahasiswa_list(page)
        npm_list = [friend['npm'] for friend in friends]
        response = {"mahasiswa_list": mahasiswa_list, "friend_list": friends, 
            "author": AUTHOR, "npm_list": npm_list, "page":page, "max_page":MAX_PAGE, "next_page": page+1, "prev_page": page-1}
        return render(request, StudentsView.template_name, response)
    