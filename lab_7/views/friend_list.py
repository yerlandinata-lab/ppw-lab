import json
from django.shortcuts import render
from django.views.generic.base import TemplateView
from django.core import serializers

from lab_7.models import Friend

AUTHOR = 'Yudhistira Erlandinata'

class FriendListView(TemplateView):
    
    template_name = 'lab_7/daftar_teman.html'

    def get(self, request, page=1, *args, **kwargs):
        data = serializers.serialize('json', Friend.objects.all())
        friends = json.loads(data)
        friends = [f['fields'] for f in friends]
        response = {"friend_list": friends, "author": AUTHOR}
        return render(request, FriendListView.template_name, response)