from .students import StudentsView
from .friends import FriendsAPI
from .friend_list import FriendListView
from .friend_detail import FriendDetailView
