import json
from django.shortcuts import render, HttpResponse
from django.views.generic.base import TemplateView
from django.core import serializers

from lab_7.models import Friend
from lab_7.api_cs_ui_helper import CSUIhelper

AUTHOR = 'Yudhistira Erlandinata'

class FriendDetailView(TemplateView):
    template_name = 'lab_7/detail_teman.html'

    def get(self, request, npm=None, *args, **kwargs):
        query = Friend.objects.filter(npm=npm)
        if query:
            struct = json.loads(serializers.serialize('json', query))
            friend = struct[0]['fields']
            data_csui = CSUIhelper().instance.get_mahasiswa(npm)
            if data_csui:
                friend['address'] = data_csui['alamat_mhs']
                friend['city'] = data_csui['kota_lahir']
                friend['birth_date'] = data_csui['tgl_lahir']
                friend['major'] = data_csui['program'][0]['nm_org'] + ' ' + data_csui['program'][0]['nm_prg'] 
            response = {'friend' : friend, 'author' : AUTHOR}
            return render(request, FriendDetailView.template_name, response)
        else:
            return HttpResponse(status=404)
