import os
import json
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.views.decorators.csrf import csrf_exempt
from django.views.generic.base import TemplateView
from django.core import serializers
from django.db.utils import IntegrityError

from lab_7.models import Friend

class FriendsAPI(TemplateView):

    def get(self, request, npm=None, *args, **kwargs):
        if npm:
            query = Friend.objects.filter(npm=npm)
            if query:
                struct = json.loads(serializers.serialize('json', query))
                data = json.dumps(struct[0]['fields'])
                return JsonResponse(data, safe=False)
            else:
                return HttpResponse(status=404)
        data = serializers.serialize('json', Friend.objects.all())
        struct = json.loads(data)
        data = json.dumps([d['fields'] for d in struct])
        return JsonResponse(data, safe=False)

    def post(self, request, *args, **kwargs):
        name = request.POST['name']
        npm = request.POST['npm']
        friend = Friend(name=name, npm=npm)
        if Friend.objects.filter(npm=npm):
            return HttpResponseBadRequest('{"error": "Mahasiswa sudah ada"}')
        friend.save()
        return HttpResponse(status=201)

    def delete(self, request, npm=None, *args, **kwargs):
        if npm is None:
            return HttpResponseBadRequest()
        query = Friend.objects.filter(npm=npm)
        if query:
            query[0].delete()
            return HttpResponse()
        else:
            return HttpResponse(status=404)