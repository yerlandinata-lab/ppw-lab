import requests
import os
from base64 import b64decode

API_MAHASISWA_LIST_URL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa-list/"
API_MAHASISWA_DETAIL = "https://api.cs.ui.ac.id/siakngcs/mahasiswa/"

class CSUIhelper:
    class __CSUIhelper:
        def __init__(self):
            self.username = os.environ['SSO_USERNAME']
            self.password = b64decode(bytes(os.environ['SSO_PASSWORD'], 'utf8')).decode('utf8') # password is encoded so people cannot steal it with naked eye
            self.client_id = 'X3zNkFmepkdA47ASNMDZRX3Z9gqSU1Lwywu5WepG'
            self.access_token = self.get_access_token()

        def get_access_token(self):         
            url = "https://akun.cs.ui.ac.id/oauth/token/"

            payload = "username=" + self.username + "&password=" + self.password + "&grant_type=password"
            headers = {
                'authorization': "Basic WDN6TmtGbWVwa2RBNDdBU05NRFpSWDNaOWdxU1UxTHd5d3U1V2VwRzpCRVFXQW43RDl6a2k3NEZ0bkNpWVhIRk50Ymg3eXlNWmFuNnlvMU1uaUdSVWNGWnhkQnBobUU5TUxuVHZiTTEzM1dsUnBwTHJoTXBkYktqTjBxcU9OaHlTNGl2Z0doczB0OVhlQ3M0Ym1JeUJLMldwbnZYTXE4VU5yTEFEMDNZeA==",
                'cache-control': "no-cache",
                'content-type': "application/x-www-form-urlencoded"
            }

            response = requests.request("POST", url, data=payload, headers=headers)
            return response.json()["access_token"]
            
        def get_mahasiswa_list(self, page=1):
            response = requests.get(API_MAHASISWA_LIST_URL,
                                    params={"access_token": self.access_token, "client_id": self.client_id, "page":page})
            mahasiswa_list = response.json()["results"]
            return mahasiswa_list

        def get_mahasiswa(self, npm):
            response = requests.get(API_MAHASISWA_DETAIL + npm + '/',
                                    params={"access_token": self.access_token, "client_id": self.client_id})
            if response.status_code == 404:
                return None
            else:
                return response.json()

    instance = None

    def __init__(self):
        if not CSUIhelper.instance:
            CSUIhelper.instance = CSUIhelper.__CSUIhelper()
