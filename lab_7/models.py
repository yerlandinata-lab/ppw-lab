from django.db import models

class Friend(models.Model):
    name = models.CharField(max_length=400)
    npm = models.CharField(max_length=250, unique=True)
    added_at = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.name + ', ' + self.npm
