from django.conf.urls import url
from .views import StudentsView, FriendsAPI, FriendListView, FriendDetailView

urlpatterns = [
        url(r'^$', StudentsView.as_view(), name='index'),
        url(r'^students$', StudentsView.as_view(), name='students'),
        url(r'^students/page/(?P<page>[0-9]+)/', StudentsView.as_view(), name='students-page'),
        url(r'^friends$', FriendsAPI.as_view(), name='friends'),
        url(r'^friends/npm/(?P<npm>[0-9X-Z]+)/', FriendsAPI.as_view(), name='friends-npm'),
        url(r'^friend-list', FriendListView.as_view(), name="friend-list"),
        url(r'^friend-detail/(?P<npm>[0-9X-Z]+)', FriendDetailView.as_view(), name='friends-detail'),
]
