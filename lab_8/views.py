from django.shortcuts import render

AUTHOR = 'Yudhistira Erlandinata'

# Create your views here.
def index(request):
    response = {'author': AUTHOR}
    return render(request, 'lab_8/lab_8.html', response)
