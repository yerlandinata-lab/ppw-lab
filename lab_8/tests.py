from django.test import TestCase, Client

# Create your tests here.
class Lab8UnitTest(TestCase):

    def test_lab8_exists(self):
        response = Client().get('/lab-8/')
        self.assertEqual(response.status_code, 200)
